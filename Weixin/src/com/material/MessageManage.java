package com.material;

import com.weixin.util.MessageUtil;

public class MessageManage {
	public String SubscribeMes(){
		StringBuffer buffer = new StringBuffer();
		buffer.append("###欢迎关注###\n\n");
		buffer.append("回复 任意，返回相同的回复\n\n");
		buffer.append("按 1  麻将房费计算\n");
		buffer.append("按 2  返回菜单2\n");
		buffer.append("按 3  图文消息测试\n");
		buffer.append("按 4  会得到一张图片\n\n");
		buffer.append("发送地理位置 得到经纬度消息\n\n");
		buffer.append("按 ?  返回主菜单");
		return buffer.toString();
	}
	
	public String UcouponSub(){
		StringBuffer buffer = new StringBuffer();
//		buffer.append("优加油--移动互联网加油利器\n\n");
//		buffer.append("※首次关注，送您3元微信公众号专属优惠券\n");
//		buffer.append("※下载优加油APP，更多优惠劵和返利在手，加油更便捷优惠\n");
//		buffer.append("※点击“一键加油”，赢取返利和红包");
		buffer.append("嗨！欢迎您加入优加油—— 一个能帮您省钱的加油移动应用family～\n");
		buffer.append("废话不多说，让你看看我的能耐：\n");
		buffer.append("1.首次使用微信公众号为爱车加油，秒减3元\n");
		buffer.append("2.下载优加油app，再获5元！更有新人券、红包券、摇奖券...所有大额加油费任性领\n");
		buffer.append("3.各类福利不定期发放，返利比例常常任性翻倍提高\n");
		buffer.append("4.会员进阶专享超多特惠，惊喜不断，优惠不停\n\n");
		buffer.append("点击下方“一键加油”即刻启程体验！");
		return buffer.toString();
	}
	
	public String Menu2(){
		StringBuffer buffer = new StringBuffer();
		buffer.append("业务介绍\n\n");
		buffer.append("介绍内容");
		return buffer.toString();
	}
	
	public String Menu3(){
		StringBuffer buffer = new StringBuffer();
		buffer.append("获取App\n");
		buffer.append("");
		return buffer.toString();
	}
	
	public String AutoMessage(){
		StringBuffer buffer = new StringBuffer();
		buffer.append("收到您的反馈");
		return buffer.toString();
	}
	
	public String MajiangIndrucion(){
		StringBuffer buffer = new StringBuffer();
		buffer.append("麻将房费计算\n\n");
		buffer.append("两种计算方法：\n");
		buffer.append("1是摊房费不赢钱\n2是兑钱买牌，结账时，在兑的钱里扣除房费后，再按输赢比例分剩下的\n\n");
		buffer.append("规则：类型 东 南 西 北 房费 \n中间空格划分\n\n");
		buffer.append("输入例子：1 8 12 5 15 50\n代表按第一种算法计算，依次输入东 南 西 北最后的牌数\n\n");
		buffer.append("一定要按上面规则输入，否则得不到结果\n");
		return buffer.toString();
	}
	
	public String repeat(String toUserName, String fromUserName, String content){
		StringBuffer contentTemp = new StringBuffer();
		contentTemp.append("你刚才说了:\n\n" + content);
		contentTemp.append("\n\n");
		contentTemp.append("我说得对吧！\n\n");
		contentTemp.append("按？试试");
		return MessageUtil.initText(toUserName, fromUserName, contentTemp.toString());
	}
}
