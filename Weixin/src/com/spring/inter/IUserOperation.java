package com.spring.inter;

import com.spring.dao.User;

/**
 * Created by Administrator on 2018/01/18 0018.
 */
public interface IUserOperation {
    public User selectUserByID(int id);
}
