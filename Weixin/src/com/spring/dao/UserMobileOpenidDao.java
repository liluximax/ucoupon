package com.spring.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Administrator on 2017/12/07 0007.
 */
@Repository
public class UserMobileOpenidDao {

    @Autowired
    @Qualifier("ucouponnamedjdbc")
    private NamedParameterJdbcTemplate namedParameterJdbcTemplateUcoupon;

    @Autowired
    @Qualifier("ucouponjdbc")
    private JdbcTemplate jdbcTemplateUcoupon;

    public String getMobileByOpenid_indb(String _openid){

        String sql = "select mobile from wx_front_end.openid_mobile where openid = ? order by id desc limit 1";

        try{
          return jdbcTemplateUcoupon.queryForObject(sql, new Object[]{_openid}, String.class);
        } catch (Exception e){
            return "NA";
        }

    }

    public void insertMobileByOpenid_indb(String openid, String mobile){
        String sql = "insert into wx_front_end.openid_mobile (openid, mobile) values (:o, :m)";
        MapSqlParameterSource source = new MapSqlParameterSource().addValue("o", openid).addValue("m", mobile);
        try{
            namedParameterJdbcTemplateUcoupon.update(sql, source);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
