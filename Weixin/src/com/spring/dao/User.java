package com.spring.dao;

import org.apache.ibatis.type.Alias;

/**
 * Created by Administrator on 2018/01/18 0018.
 * 用来做mybatis的测试类
 * 2018/01/18
 * 工程中没有用
 */
@Alias("User")
public class User {
    String name;
    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

