package com.spring.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018/01/05 0005.
 */
@Repository
public class ScanEventKeyDao {
    @Autowired
    @Qualifier("ucouponnamedjdbc")
    private NamedParameterJdbcTemplate namedParameterJdbcTemplateUcoupon;

    @Autowired
    @Qualifier("ucouponjdbc")
    private JdbcTemplate jdbcTemplateUcoupon;

    /**
     * 已废弃
     * @param openid
     * @param ten_minutes_later
     * @return
     */
    public int isExist_EK_fromdb(String openid, String ten_minutes_later){
        String sql = "select count(*) from wx_front_end.scan_eventkey where update_time > ? AND openid = ?";
        try {
            return jdbcTemplateUcoupon.queryForObject(sql, new Object[]{ten_minutes_later, openid}, Integer.class);
        }catch (Exception e){
            e.printStackTrace();
            return  -1;
        }
    }

    /**
     * 已废弃
     * @param openid
     * @return
     */
    public String get_EK_fromdb(String openid){
        String sql = "select eventkey from wx_front_end.scan_eventkey where openid = ? order by id desc limit 1";
        try {
            String result = jdbcTemplateUcoupon.queryForObject(sql, new Object[]{openid}, String.class);
            System.out.println("查询到的场景id为：" + result);
            return  result;
        }catch (Exception e){
            e.printStackTrace();
            return "0";
        }
    }

    public void insert_EK_intodb(String openid, String eventKey){

        String sql = "insert into wx_front_end.scan_eventkey (openid, eventkey) values (:o, :e)";
        MapSqlParameterSource source = new MapSqlParameterSource().addValue("o", openid).addValue("e", eventKey);
        try{
            namedParameterJdbcTemplateUcoupon.update(sql, source);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public int openidIsExist(String openid){
        String sql = "select count(*) from wx_front_end.scan_eventkey where openid = ?";
        try{
            return jdbcTemplateUcoupon.queryForObject(sql, new Object[]{openid}, Integer.class);
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    public Map<String, String> queryUTime_EK(String openid){
        String sql = "select update_time, eventKey from wx_front_end.scan_eventkey where openid = ?";
        HashMap<String, String> map = new HashMap<>();
        try{

            jdbcTemplateUcoupon.query(sql, new Object[]{openid}, new RowCallbackHandler() {
                @Override
                public void processRow(ResultSet rs) throws SQLException {
                    map.put("update_time", rs.getString("update_time"));
                    map.put("event_key", rs.getString("eventkey"));
                }
            });
            return map;

        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public void deleteEKInfo(String openid){
        String sql = "delete from wx_front_end.scan_eventkey where openid = :openid";
        try {
            MapSqlParameterSource source = new MapSqlParameterSource().addValue("openid", openid);
            namedParameterJdbcTemplateUcoupon.update(sql, source);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
