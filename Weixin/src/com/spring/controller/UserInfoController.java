package com.spring.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.spring.service.AccessTokenService;
import com.spring.service.EventKeyService;
import com.spring.service.UserOpenidMobileService;
import com.weixin.pay.domain.BdyzInfo;
import com.weixin.pay.service.DateUtils;
import com.weixin.pay.util.CommonUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.weixin.model.token.JsSignature;
import com.weixin.util.DeveloperId;
import com.weixin.util.WeixinUtil;
import com.weixin.util.jssdk.JsUtil;

import net.sf.json.JSON;
import net.sf.json.JSONObject;
import org.springframework.web.client.RestTemplate;

@Controller
@RequestMapping(value = "userinfo")
public class UserInfoController {


    @Autowired
    private JsUtil jsUtil;

    @Autowired
    private Logger logger;

    private static final String ACCESS_TOKEN_URL_WEB = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";

    private static final String USER_INFO_URL = "https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";

    private static final String QUERY_ORDERS_URL = "https://app1.u-coupon.cn:448/new_wx/Query_log.php?openid=OPENID&addtime=ADDTIME&deltime=DELTIME";

    private static final String LOGIN_VALIDATE = "http://app1.u-coupon.cn:8000/new_wx/login.php?mobile=MOBILE&authcode=CODE";


    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private EventKeyService eventKeyService;

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

//    @Autowired
//    private AccessTokenCache accessTokenCache;

    @Autowired
    private AccessTokenService accessTokenService;

    @Autowired
    private UserOpenidMobileService userOpenidMobileService;

    @RequestMapping(value = "access_token")
    @ResponseBody
    public Map<String, String> fetchUcouponAccessToken(){
        Map<String, String> token = new HashMap<>();
        token.put("token", accessTokenService.getAccessToken());
        return token;
    }

    @RequestMapping(value = "getuserinfo")
    public void getCode(@RequestParam("code") String _code,
                        @RequestParam(value = "state", required = false) String _state,
                        HttpServletResponse response,
                        HttpSession session) throws IOException {
        String code = _code;
        String state = _state;
        /**
         * 通过code换取网页授权access_token
         *
         */
        String access_token_url = ACCESS_TOKEN_URL_WEB.replace("APPID", DeveloperId.APPID)
                .replace("SECRET", DeveloperId.APPSECRET).
                        replace("CODE", code);
        JSONObject access_token_data = WeixinUtil.doGetStr(access_token_url);
        try {
            String access_token = access_token_data.getString("access_token");
            String refresh_token = access_token_data.getString("refresh_token");
            String openid = access_token_data.getString("openid");
            String scope = access_token_data.getString("scope");
//            System.out.println("openid: " + openid);
//            logger.info("openid: " + openid);
            /**
             * 拉取用户信息
             *
             */
            String user_info_url = USER_INFO_URL.replace("ACCESS_TOKEN", access_token).replace("OPENID", openid);
            JSON userInfo = WeixinUtil.doGetStr(user_info_url);
            JSONObject userInfoObj = JSONObject.fromObject(userInfo);
            String image = userInfoObj.getString("headimgurl");
            System.out.println("UserInfoController中打印的imageurl: " + image);

            session.setAttribute("imageurl", image);

            response.setContentType("text/html;charset=utf-8");
            PrintWriter out = response.getWriter();
            out.print(userInfo);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            // TODO: handle exception
        }
//		String nickname = userInfo.getString("nickname");
//		String headimgurl = userInfo.getString("headimgurl");
//		System.out.println(nickname+","+headimgurl);
    }

    @RequestMapping("jssdk")
    public void getJssdk(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=utf-8");
        PrintWriter out = response.getWriter();
        String url = request.getParameter("url");
        JsSignature signature = jsUtil.sign(url);
        JSONObject result = JSONObject.fromObject(signature);
        String out_result = result.toString();
        System.out.println("fetch jssdk json\n");
        logger.info("----[1]---- fetch jssdk json: " + out_result + "\n");
        out.print(out_result);
    }

    @RequestMapping(value = "getopenid")
    public void getOpenId(@RequestParam("code") String _code,
                          @RequestParam(value = "state", required = false) String _state,
                          HttpServletResponse response, HttpServletRequest request){
        String code = _code;
        String state = _state;
        HttpSession session = request.getSession();
        Map<String,String> oauth = new HashMap<>();
        String openid = new String();

        try {
            String codefromSess = (String) session.getAttribute("code");
            System.out.println("code from session: " + codefromSess);
            logger.info("----[1]----code from session: " + codefromSess);

            if (codefromSess != null && codefromSess.equals(code)){
                openid = (String) session.getAttribute("openid");
                System.out.println("----[1]----code已经存在，从session中获取到openid :" + openid);
                logger.info("----[1]----code已经存在，从session中获取到openid :" + openid);

            }else{
                session.setAttribute("code", code);
                /**
                 * 通过code换取网页授权access_token
                 *
                 */
                System.out.println("----[1]----从URL中 获取的参数code： " +   code);
                logger.info ("----[1]----从URL中 获取的参数code： " +   code);
                String access_token_url = ACCESS_TOKEN_URL_WEB.replace("APPID", DeveloperId.APPID)
                        .replace("SECRET", DeveloperId.APPSECRET).
                                replace("CODE", code);

                String result = CommonUtils.httpRequestByGet(access_token_url);
                com.alibaba.fastjson.JSONObject access_token_data = com.alibaba.fastjson.JSON.parseObject(result);

                logger.info("----[1]----通过code换取网页授权access_token: " + access_token_data);
                openid = access_token_data.getString("openid");

                if(openid == null){
                    logger.error("----[1]----通过code换取网页授权access_token: " + access_token_data);
                    logger.error("----[1]----获取到的openid为空！！");
                }
                logger.info("----[1]-----获取到openid: " + openid);
                if (openid != null){
                    session.setAttribute("openid", openid);
                }
                System.out.print("获取的openid: " + openid + ", at ");
                DateUtils.printTimeStamp();

            }

            session.setMaxInactiveInterval(3 * 60);// session保存时间为3分钟

            oauth.put("openid", openid);
            PrintWriter out = response.getWriter();
            out.print(com.alibaba.fastjson.JSON.toJSONString(oauth));
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * writeby hl
     * 2017/05/15
     * @param openid
     * @param addtime
     * @param deltime
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "getordersinfo")
    public void getDealInfo(
                            @RequestParam(value = "openid") String openid,
                            @RequestParam(value = "addtime") String addtime,
                            @RequestParam(value = "deltime") String deltime,
                            HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=utf-8");
        PrintWriter out = response.getWriter();
        String url = QUERY_ORDERS_URL.replace("OPENID", openid).replace("ADDTIME", addtime).replace("DELTIME", deltime);
        System.out.println(url);
        com.alibaba.fastjson.JSONObject ordersInfoObj = com.alibaba.fastjson.JSON.parseObject(restTemplate.getForObject(url, String.class));
        Integer code = ordersInfoObj.getInteger("code");
        String detail = ordersInfoObj.getString("detail");
        if (code == 204) {
            System.out.println(detail);
        }
        out.print(ordersInfoObj);
    }

    /**
     * 首次登录验证
     * writeby whl
     * @param json
     * @return
     * @throws UnsupportedEncodingException
     */
    @RequestMapping(value = "login", method = RequestMethod.POST)
    @ResponseBody
    public String userLogin(@RequestBody String json) throws UnsupportedEncodingException {
        json = URLDecoder.decode(json, "utf-8");
        int index = json.indexOf("=");
        if (index != -1)
            json = json.substring(0, json.indexOf("="));
        BdyzInfo data = com.alibaba.fastjson.JSON.parseObject(json, BdyzInfo.class);//因为绑定油站的类与此处要求相符，所以继续沿用
        System.out.println("[从页面传来的用户登录信息]: " + data);
        String code = data.getAuth_code();
        String mobile = data.getMobile();
        String url = LOGIN_VALIDATE.replace("MOBILE", mobile).replace("CODE", code);
        String result = restTemplate.getForObject(url, String.class);
        System.out.println("result :" + result);
        return com.alibaba.fastjson.JSON.toJSONString(result);

    }


    /**
     * 登录后 将openid mobile 插入数据库
     * writeby hl
     * 已废弃
     * @param _openid
     * @param _mobile
     * @param request
     * @param response
     * @throws UnsupportedEncodingException
     */
    @RequestMapping(value = "insertOM")
    public void insertUserOMInfoToDB(@RequestParam("openid") String _openid,
                                   @RequestParam("mobile") String _mobile,
                                   HttpServletRequest request,
                                   HttpServletResponse response) throws UnsupportedEncodingException {
        String openid = _openid;
        String mobile = _mobile;
        userOpenidMobileService.insertMobileByOpenid(openid, mobile);
    }


    /**
     * 从数据库中查询用户mobile
     * 已废弃
     * writeby hl
     * @param openid
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "getOM")
    public void getUserOMInfoFromDB(@RequestParam("openid") String openid,
                                    HttpServletRequest request,
                                    HttpServletResponse response) throws IOException {
        Map<String, String> map = new HashMap<>();
        String mobile =  userOpenidMobileService.getMobileByOpenid(openid);
        map.put("mobile", mobile);
        PrintWriter out = response.getWriter();
        out.print(com.alibaba.fastjson.JSON.toJSONString(map));
    }


    /**
     * 将openid和mobile设置到cookie中
     * @param mobile
     * @param request
     * @param response
     */
    @RequestMapping(value = "addIntoCookie")
    public void addIntoCookie(@RequestParam("mobile") String mobile,
                              HttpServletRequest request,
                              HttpServletResponse response){

        Cookie[] cookies = request.getCookies();
        if (cookies == null){
            System.out.println("Cookie 保存内容已过期... 或者 有新的用户登录");
        }else{
            for (Cookie cookie : cookies){
                if (cookie.getName().equals("mobile")){
                    System.out.println("被删除的cookie的名字为：" + cookie.getName() +
                            "被删除的cookie的值为：" + cookie.getValue());
                    cookie.setValue(null);
                    cookie.setMaxAge(0);
                    cookie.setPath("/");
                }
            }
        }
        Cookie cookie = new Cookie("mobile", mobile);
        cookie.setMaxAge(7*24*60*60);
        cookie.setPath("/");
        response.addCookie(cookie);
    }

    /**
     * 从cookie中获取用户手机号
     * 如果cookie中不存在，则设置为NA
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "getFMCookie")
    public void getFMCookie(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Cookie[] cookies = request.getCookies();
        HashMap<String,String> map = new HashMap<>();
        if (cookies == null || cookies.length == 0){
            System.out.println("Cookie 保存内容已过期...");
            map.put("mobile", "NA");
        }else {
            for (Cookie cookie : cookies){
                if (cookie.getName().equals("mobile")){
                    System.out.println("mobile:" + cookie.getValue());
                    map.put("mobile", cookie.getValue());
                }else{
                    map.put("mobile", "NA");
                }
            }
        }
        PrintWriter out = response.getWriter();
        out.print(com.alibaba.fastjson.JSON.toJSONString(map));
    }

    /**
     * 根据openid查eventKey
     * @param request
     * @param response
     * @param openid
     * @throws IOException
     */
    @RequestMapping(value = "getEventKey")
    public void get_EventKey_FMDB(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "openid") String openid) throws IOException, ParseException {
        String eventKey = eventKeyService.getEventKey(openid);
        System.out.println("获取到的eventKey:" + eventKey);
        HashMap<String, String> map = new HashMap<>();
        map.put("eventKey", eventKey);
        PrintWriter out = response.getWriter();
        out.print(com.alibaba.fastjson.JSON.toJSONString(map));
    }

} 
