package com.spring.controller;

import com.spring.dao.ScanEventKeyDao;
import com.spring.service.EventKeyService;
import com.weixin.pay.dao.TransationDao;
import com.weixin.pay.domain.generateOrder.OrderData;
import com.weixin.pay.service.AsyncService;
import com.weixin.pay.service.CacheService;
import com.weixin.pay.service.RedisService;
import org.apache.http.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Administrator on 2017/12/30 0030.
 *
 * 测试用的controller
 */
@Controller
public class TestController {

    @Autowired
    AsyncService asyncService;
//    @Autowired
//    EventKeyService service;
//
//    @RequestMapping(value = "test/query_for_db")
//    public void test_for_db(){
//        String openid = "oNTV9wS72vrgzK9EI4-BGVIXJeM0";
//        String eventKey = "111";
////        service.insert_eventkey(openid, eventKey);
//        String string = service.get_event_key(openid);
//        System.out.println(string);
//    }
//
//    @Autowired
//    TransationDao transationDao;

//    @RequestMapping(value = "test/query_for_db")
//    public void test_for_db(){
//        SimpleDateFormat timeStamp =  new SimpleDateFormat( "yyyy-MM-dd" );
//		long create_time = System.currentTimeMillis();
//		String today = timeStamp.format(create_time);
//        ArrayList<String> list = transationDao.query_userful_openid(today);
//        for (String openid: list
//             ) {
//            System.out.println(openid);
//        }
//    }


//    @Autowired
//    RedisService redisService;
//
//    @Autowired
//    private CacheService cacheService;
//
//    @RequestMapping(value = "get_from_redis")
//    @ResponseBody
//    public OrderData getOrderDataFromCache(@RequestParam(value = "out_trade_no") String out_trade_no){
//        OrderData orderData = new OrderData();
//        orderData = redisService.handleByRedisCache(out_trade_no, orderData);
//        System.out.println("从Redis中获取的订单信息，订单号： " + out_trade_no + " from redis " + orderData);
//        return orderData;
//
////        OrderData orderData = new OrderData();
////        orderData = cacheService.handleOrderDateByCache(out_trade_no, orderData);
////        System.out.println("----[11]----从缓存中取订单信息， 订单号： " + out_trade_no + " from cache " + orderData);
////        return orderData;
//    }

//    @RequestMapping(value = "get_code_url")
//    public  void get_code_url_fmdb(){
//        for (int i = 1; i < 79; i++) {
//            String code_url = transationDao.get_code_str(i);
////            System.out.println(code_url);
//            String code_str = code_url.substring(23, code_url.length());
////            System.out.println(code_str);
//            transationDao.update_code_str(code_str, i);
//        }
//    }
//    @Autowired
//    ScanEventKeyDao scanEventKeyDao;

//    @Autowired
//    EventKeyService eventKeyService;
//
//    @RequestMapping(value = "get_ek_fdb")
//    public void getEKFDB() throws ParseException {
//        String openid = "oNTV9wc0E43bUooKmtYYuT3GG_VM";
//        long nowTime = System.currentTimeMillis();
//        System.out.println("nowTime: " + nowTime);
//        long ten_minutes_later = nowTime - 600 * 10;
//        System.out.println("ten minutes before: " + ten_minutes_later);
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String ten_minutes_str = sdf.format(ten_minutes_later);
//        String event_key = "0";
//        int isExist = scanEventKeyDao.isExist_EK_fromdb(openid, ten_minutes_str);
//        System.out.println("ten_minutes_str: " + ten_minutes_str);
//        System.out.println("isExist：" + isExist);
//        String  eventKey = eventKeyService.getEventKey(openid);
//        System.out.println(eventKey);
//    }

    @RequestMapping(value = "/time_out_test")
    @ResponseBody
    public void test(HttpServletResponse response) throws IOException {
//        asyncService.testAsync();
        response.getWriter().write("OK");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("睡了10s");
    }

}
