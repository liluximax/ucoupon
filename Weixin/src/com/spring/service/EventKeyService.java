package com.spring.service;

import com.spring.dao.ScanEventKeyDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 2018/01/05 0005.
 */
@Service
public class EventKeyService {

    @Autowired
    ScanEventKeyDao scanEventKeyDao;

    /**
     * 获取evenKey 已废弃
     * @param openid
     * @param eventKey
     */
//    public String get_event_key(String openid){
//        long nowTime = System.currentTimeMillis();
//        long ten_minutes_later = nowTime - 600 * 1000;
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String ten_minutes_str = sdf.format(ten_minutes_later);
//        String event_key = "0";
//        int isExist = scanEventKeyDao.isExist_EK_fromdb(openid, ten_minutes_str);
//        if (isExist >= 1){
//            event_key = scanEventKeyDao.get_EK_fromdb(openid);
//        }
//        return  event_key;
//    }

    /**
     * 记录扫码 eventKey 到数据库
     * @param openid
     * @param eventKey
     */
    public void insert_eventkey(String openid, String eventKey){
        scanEventKeyDao.insert_EK_intodb(openid, eventKey);
    }

    /**
     * 判断数据库中是否存在对应的openid记录
     * @param openid
     * @return
     */
    public boolean isExistIndb(String openid){
        int isExist = scanEventKeyDao.openidIsExist(openid);
        if(isExist >= 1){
            return true;
        }else {
            return false;
        }
    }

    /**
     * 10分钟内的eventKey 设计为有效
     * 10分钟以外的进行删除逻辑， 返回0
     * @param openid
     * @return
     * @throws ParseException
     */
    public String getEventKey(String openid) throws ParseException {
        String event_key = "0";
        boolean isExist = isExistIndb(openid);
        if (isExist){
            //存在对应的openid信息
            HashMap<String, String> map = (HashMap<String, String>) scanEventKeyDao.queryUTime_EK(openid);//map 记录最后一次的update_time  和 eventKey
            if (map != null){
                String update_time = map.get("update_time");
                event_key = map.get("event_key");
                long nowTime = System.currentTimeMillis();
                long ten_minutes_before = nowTime - 600 * 1000;
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                long update_time_stamp = sdf.parse(update_time).getTime();
                if (ten_minutes_before < update_time_stamp){//在十分钟以内， eventKey有效
                    String pattern = "^[0-9]{1,6}";
                    boolean isMatch = Pattern.matches(pattern, event_key);
                    if (isMatch){
                        return event_key;
                    }else {
                        return "0";
                    }
                }else{
                    scanEventKeyDao.deleteEKInfo(openid);//十分钟以外无效，删除eventKey的记录信息
                    return "0";
                }

            }else {
                return "0";
            }

        }else{
            return event_key;
        }

    }


}
