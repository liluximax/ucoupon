package com.spring.service;

import com.spring.dao.UserMobileOpenidDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2017/12/07 0007.
 */
@Service
public class UserOpenidMobileService {
    @Autowired
    private  UserMobileOpenidDao userMobileOpenidDao;

    public String getMobileByOpenid(String openid){
        String mobile = userMobileOpenidDao.getMobileByOpenid_indb(openid);
        return  mobile;
    }

    public void insertMobileByOpenid(String openid, String mobile){
        userMobileOpenidDao.insertMobileByOpenid_indb(openid, mobile);
    }

}
