package com.spring.aop;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;

import java.lang.reflect.Modifier;

@Aspect
@Component
public class SendWXOrderAspect {

    @Pointcut(value = "execution(* com.spring.aop.testdemo.*.*(..))")
    private  void pointCut(){

    }

    /**
     * 前置方法,在目标方法执行前执行
     * @param joinPoint 封装了代理方法信息的对象,若用不到则可以忽略不写
     */
    @Before("pointCut()")
    public void beforeMethod(JoinPoint joinPoint){
        System.out.println("目标方法名为:" + joinPoint.getSignature().getName());
        System.out.println("目标方法所属类的简单类名:" +        joinPoint.getSignature().getDeclaringType().getSimpleName());
        System.out.println("目标方法所属类的类名:" + joinPoint.getSignature().getDeclaringTypeName());
        System.out.println("目标方法声明类型:" + Modifier.toString(joinPoint.getSignature().getModifiers()));
        //获取传入目标方法的参数
        Object[] args = joinPoint.getArgs();
        for (int i = 0; i < args.length; i++) {
            System.out.println("第" + (i+1) + "个参数为:" + args[i]);
        }
        System.out.println("被代理的对象:" + joinPoint.getTarget());
        System.out.println("代理对象自己:" + joinPoint.getThis());
    }

    /**
     * 环绕方法,可自定义目标方法执行的时机
     * @param pjd JoinPoint的子接口,添加了
     *            Object proceed() throws Throwable 执行目标方法
     *            Object proceed(Object[] var1) throws Throwable 传入的新的参数去执行目标方法
     *            两个方法
     * @return 此方法需要返回值,返回值视为目标方法的返回值
     */

//    @Around(value = "pointCut()")
//    public Object aroundMethod(ProceedingJoinPoint pjd){
//        Object result = null;
//        try {
//            //前置通知
//            System.out.println("目标方法执行前...");
//            //执行目标方法
//            //result = pjd.proeed();
//            //用新的参数值执行目标方法
//            result = pjd.proceed(new Object[]{"newSpring","newAop"});
//            //返回通知
//            System.out.println("目标方法返回结果后...");
//        } catch (Throwable e) {
//            //异常通知
//            System.out.println("执行目标方法异常后...");
//            throw new RuntimeException(e);
//        }
//        //后置通知
//        System.out.println("目标方法执行后...");
//
//        return result;
//    }

    /**
     * 使用动态代理对请求方法进行拦截处理
     * 代理的方法   就在下面    不过注释掉了
     * 供参考，提供一种思路
     * whl
     * @param pjd
     * @return
     */
//    @Around(value = "pointCut()")
//    public String  aroundMethod(ProceedingJoinPoint pjd){
//        Object result = null;
//        int a = 0;
//        while(true){
//            try {
//                result = pjd.proceed();
//                if (result != null){
//                    break;
//                }
//            }catch (ResourceAccessException e){
//                System.out.println("提交订单信息，请求超时");
//                a ++;
//                try{
//                    if(a < 6){
//                        Thread.sleep(Math.round(Math.pow(2,a-1))*1000);
//                    }else if(a < 11){
//                        Thread.sleep(16*1000);
//                    }else{
//                        break;
//                    }
//                }catch (InterruptedException e1){
//                    System.out.println("线程sleep中断");
//                }
//            }catch (Throwable throwable){
//                throw new RuntimeException(throwable);
//            }
//        }
//        return (String) result;
//    }

    /**
     * 上文的代理方法
     */
//    private String sendWxOrderDataWithCatchException_AOP(String url, String data){
//        String result = null;
//        if (data == null){
//            result = restTemplate.getForObject(url, String.class);
//        }else {
//            result = restTemplate.postForObject(WX_DIR_ORDER, data, String.class);
//        }
//    }
}
