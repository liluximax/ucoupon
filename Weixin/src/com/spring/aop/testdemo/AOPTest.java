package com.spring.aop.testdemo;

import org.springframework.stereotype.Component;

@Component("AOPTest")
public class AOPTest {

    public String joint(String str1, String  str2){
        return str1 + str2;
        }

        }
