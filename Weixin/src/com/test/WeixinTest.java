package com.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.spring.aop.testdemo.AOPTest;
import com.weixin.pay.dao.TransationDao;
import com.weixin.pay.domain.generateOrder.ComOrderData;
import com.weixin.pay.domain.generateOrder.DirOrderData;
import com.weixin.pay.domain.generateOrder.OrderData;
import com.weixin.pay.service.AdminService;
import com.weixin.pay.service.impl.AdminServiceImpl;
import com.weixin.util.DeveloperId;
import com.weixin.util.MessageUtil;
import com.weixin.util.WeixinUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * 菜单创建以及查询
 */
@Log4j2
public class WeixinTest {

	private static final String PAY_URL = "";
	private static final String WX_DIR_ORDER = "http://app1.u-coupon.cn:8000/Scaven/Sca_order.php";

	public static void main(String[] args) throws InterruptedException{

//		String access_token_url = "http://nwxoa.u-coupon.cn/Weixin/userinfo/access_token.do";
//		RestTemplate restTemplate = new RestTemplate();
//		String token = JSON.parseObject(restTemplate.getForObject(access_token_url, String.class)).getString("token");

		/**
		 * 创建菜单
		 */
//		String menu = JSONObject.fromObject(WeixinUtil.initMenu3()).toString();
//		String menu = JSONObject.toJSONString(WeixinUtil.initMenu3());
//		/**
//		 * 维护菜单
//		 */
//		String menu = JSONObject.fromObject(WeixinUtil.fixMenu()).toString();

//		int result = WeixinUtil.creatMenu(token, menu);
//		if(result == 0){
//			System.out.println("菜单创建成功");
//		}
//		else{
//			System.out.println("错误码是: " + result);
//		}
		
		/**
		 * 查询菜单
		 */

//		System.out.println(WeixinUtil.queryMenu(token));
		
		/** 
		 * 设置行业信息 及查询
		 */
//		ModelMessageUtil.setIndustry(token.getToken(), "11", "12");
//		System.out.println(ModelMessageUtil.queryIndustry(token.getToken()));
		
		/**
		 * 支付消息推送
		 */
//		PayService payService = new PayService();
//		String returncode = payService.sendPayMessage("oAC51s6M-XKhjKHbMyApTMB3gm64");
//		System.out.println(returncode);
//		String url = "https://app1.u-coupon.cn:448/new_wx/Query_log.php?openid=oNTV9wc_8-RzkM1exK9-n7BwkYlc&addtime=1494340600&deltime=1494396000%22";

//		String deal_info_url = "http://localhost:8080/Weixin/userinfo/getdealinfo.do?openid=oNTV9wc_8-RzkM1exK9-n7BwkYlc&addtime=1494340600&deltime=1494396000%22";
//		RestTemplate restTemplate = new RestTemplate();
//		restTemplate.getForObject(deal_info_url, String.class);
//		String string = "{\"code\":\"1\",\"detail\":\"\\u652f\\u4ed8\\u6210\\u529f\\uff0c\\u6b63\\u5728\\u51fa\\u7968\",\"stationname\":\"\\u4e2d\\u56fd\\u77f3\\u5316\\u6c34\\u78e8\\u6c9f\\u533a\\u521b\\u4e1a\\u56ed\\u52a0\\u6cb9\\u52a0\\u6c14\\u7ad9\",\"stationid\":\"S0200028\",\"car\":\"\\u672a\\u586b\\u5199\",\"ordernum\":\"66405967937\",\"pass\":4784,\"addtime\":1505967937,\"amount\":\"38.22\",\"cash\":35.22,\"ticket\":\"0\",\"ticketmoney\":35.22,\"umoney\":\"0.77\"}";
//		JSONObject jsonObject = JSON.parseObject(string);
//		if (jsonObject.getString("code") == ("1")){
//			System.out.println("ok");
//		}

//		AdminService adminService = new AdminServiceImpl();
//		JSONObject json = adminService.queryTodayOrdersInfo("oNTV9wSo5C4xMULmP55Ke5b7fUOQ");
//		int sizeOfOrd = adminService.getTodayOrdersSize(json);
//		String orderInfo = adminService.queryTodayOrders(json, "1513603294", 1);
//		String message = MessageUtil.initText("gh_86fc878727f4", "oNTV9wT1kKwy7l618KIr3U2t5lNg", orderInfo);
//
//		System.out.println(json.toString());
//		System.out.println(sizeOfOrd);
//		System.out.println(orderInfo);
//		System.out.println(message);
//		SimpleDateFormat timeStamp =  new SimpleDateFormat( "yyyy-MM-dd" );
//		long create_time = System.currentTimeMillis();
//		System.out.println(timeStamp.format(create_time));

//		SimpleDateFormat sdf =  new SimpleDateFormat( "yyyy-MM-dd" );
//		Date date=new Date();
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(date);
//        calendar.add(Calendar.DAY_OF_MONTH, +3);
//        date = calendar.getTime();
//		System.out.println(sdf.format(date));
//		long nowTime = System.currentTimeMillis();
//		System.out.println("nowTime: " + nowTime);
//		long ten_minutes_later = nowTime - 600 * 1000;
//		System.out.println("ten minutes before: " + ten_minutes_later);
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//		String test_date = "2018-01-16 15:20:57";
//		Date test_time = sdf.parse(test_date);
//		System.out.println("测试日期转换到时间戳" + test_time.getTime());
//
//
//		String ten_minutes_str = sdf.format(ten_minutes_later);
//		System.out.println("ten_minutes_str: " + ten_minutes_str);

//		String eventKey = "qrscene_10238";
//		String eventKey_REG = eventKey.split("_")[1];
//		System.out.println(eventKey_REG);
//		String matchStr = "11233";
//		String pattern = "^[0-9]{1,6}";
//		boolean isMatch = Pattern.matches(pattern, matchStr);
//		System.out.println(isMatch);

//		String data = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx3b4316026c47b1a5&redirect_uri=http%3A%2F%2Fnwxoa.u-coupon.cn%2FWeixin%2Fdirectpay%2Findex.html%3Fstation_id%3DS0100001%26station_name%3D%E6%96%B9%E6%81%92%E5%9B%BD%E9%99%85%E4%B8%AD%E5%BF%83&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect";
//		String data = "http://nwxoa.u-coupon.cn/Weixin/wxpay/index.html?station_id=S0100001&station_name=方恒国际中心";
//		data = URLEncoder.encode(data, "utf-8");
//		System.out.println(data);


//		HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
//		httpRequestFactory.setConnectionRequestTimeout(5000);
//		httpRequestFactory.setConnectTimeout(5000);
//		httpRequestFactory.setReadTimeout(10000);
//		RestTemplate restTemplate = new RestTemplate(httpRequestFactory);
//		Map<String, String> map = new HashMap<>();
//		String favorable = "9.00";
//		String out_trade_no = "1234434";
//		map.put("payid", out_trade_no);
//		map.put("favorable", favorable);
//		String wx_dir_json = JSON.toJSONString(map);
//		System.out.println(wx_dir_json);
//		String result = null;
//
//		int a = 0;
//		while(true){
//			try {
//				result = restTemplate.postForObject(WX_DIR_ORDER, wx_dir_json, String.class);
//				if (result != null){
//					System.out.println("result:" + result);
//					break;
//				}
//			}catch (ResourceAccessException e){
//				a ++;
//				if(a < 6){
//					Thread.sleep(Math.round(Math.pow(2,a-1))*1000);
//				}else if(a < 11){
//					Thread.sleep(16*1000);
//				}else{
//					break;
//				}
//			}
//		}
//
//		System.out.println("OK");

		ApplicationContext context = new
				ClassPathXmlApplicationContext("com/spring/config/bean.xml");
		AOPTest aopTest = (AOPTest) context.getBean("AOPTest");
		String result = aopTest.joint("spring", "aop");
		System.out.println(result);

	}

}
