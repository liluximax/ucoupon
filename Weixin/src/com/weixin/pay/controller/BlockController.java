package com.weixin.pay.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.weixin.pay.dao.TransationDao;
import com.weixin.pay.domain.BdyzInfo;
import com.weixin.pay.domain.TimeStampObj;
import com.weixin.pay.domain.modelMsg.*;
import com.weixin.pay.domain.modelMsg.exception.ParamNullException;
import com.weixin.pay.service.AdminService;
import com.weixin.pay.service.CacheService;
import com.weixin.pay.service.ModelMsg;
import com.weixin.pay.service.ModelMsgSendService;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by luc on 16/11/16.
 */
@Controller
//@Scope(value = "request")
public class BlockController {

    private TimeStampObj time = new TimeStampObj();

    private static ReentrantLock lock = new ReentrantLock();

    private static SimpleDateFormat sdf =  new SimpleDateFormat( "yyyy-MM-dd" );

    @Autowired
    private ModelMsg modelMsg;

    @Autowired
    private AdminService adminService;

    @Autowired
    private ModelMsgSendService modelMsgSendService;

    @Autowired
    private Logger logger;


//    @Autowired
//    ModelMsgScource modelMsgScource;

    @Autowired
    TransationDao transationDao;
    /**
     * 请求参数相同
     * controller会阻塞
     * 参数不同,相当于每个请求开启一个线程,不会造成阻塞
     * <p>
     * 参数相同,并不是spring阻塞,而是在chrome上测试,chrome针对同一url策略是排队请求
     *
     * @param status
     * @return
     */
    @RequestMapping(value = "block")
    @ResponseBody
    Map<String, String> getBlock(@RequestParam(value = "status") String status) {
        Map<String, String> map = new HashMap<>();
        map.put("status", "ok");
//        lock.lock();
        String threadName = Thread.currentThread().getName();
        System.out.println(threadName + " req get");
        if ("sleep".equals(status)) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else if ("sleep2".equals(status)) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(threadName + " req finished");
        map.put("obj", time.toString());
//        lock.unlock();
        return map;
    }

    @RequestMapping(value = "util/bdyz", method = RequestMethod.POST)
    @ResponseBody
    String bdOpenidWithMobile(@RequestBody String json) throws UnsupportedEncodingException {
        json = URLDecoder.decode(json, "utf-8");
        int index = json.indexOf("=");
        if (index != -1)
            json = json.substring(0, json.indexOf("="));
        System.out.println(json);
        BdyzInfo data = JSON.parseObject(json, BdyzInfo.class);
        System.out.println("[从页面传来的绑定信息]: " + data);
        return JSON.toJSONString(adminService.linkOpenidWithMobile(data.getOpenid(), data.getMobile(), data.getAuth_code()));
    }

    @RequestMapping(value = "modelmsg/ticket_delay")
    @ResponseBody
    ModelMsgReqSuccess sendTicketDelay(@RequestParam(value = "openid", required = false) String openidStr,
                                       @RequestParam(value = "out_trade_no", required = false) String out_trade_no,
                                       @RequestParam(value = "station_name", required = false) String station_name,
                                       @RequestParam(value = "amount", required = false) String amount,
                                       @RequestParam(value = "moblie", required = false) String mobile,
                                       @RequestParam(value = "license", required = false) String license,
                                       @RequestParam(value = "time_end", required = false) String time_end) throws UnsupportedEncodingException {

        if (openidStr == null || out_trade_no == null || station_name == null || amount == null || mobile == null || license == null || time_end == null) {
            throw new ParamNullException();
        }
        if (openidStr.equals("") || out_trade_no.equals("") || station_name.equals("") || amount.equals("") || mobile.equals("") || license.equals("") || time_end.equals("")) {
            throw new ParamNullException();
        }

        /**
         * server已经将GET的编码方式配成 UTF-8 故不需要此转换
         *
         * BUT server这样设置会导致log乱码,故又修改回来
         */
        station_name = new String(station_name.getBytes("iso8859-1"), "UTF-8");
        license = new String(license.getBytes("iso8859-1"), "UTF-8");

        //转换金额格式
        amount = modelMsg.change2CNY(amount);

        time_end = time_end + "00";
        //转换时间格式
        long timeStamp = Long.parseLong(time_end);
        time_end = modelMsg.changeDataFormat(timeStamp);

        List<String> openidList = new ArrayList<>();
        for (String openid : openidStr.split(",")){
            if(openid.length() > 5){
                openidList.add(openid);
            }
        }

        Map<String, String> params = new HashMap<>();
        params.put("first", time_end);
        params.put("keyword1", out_trade_no);
        params.put("keyword2", station_name);
        params.put("keyword3", amount);
        params.put("keyword4", mobile);
        params.put("keyword5", license);

        ModelMsgReqSuccess respData = modelMsgSendService.getModelMsgSendStatus(params, openidList, ModelMsgStatus.DELAY.getIndex());

        return respData;
    }
    @RequestMapping(value = "modelmsg/new_oil_transaction")
    @ResponseBody
    ModelMsgReqSuccess sendTransactionNotify(@RequestParam(value = "openid", required = false) String openidStr,
                                       @RequestParam(value = "user", required = false) String user,
                                       @RequestParam(value = "station_name", required = false) String station_name,
                                       @RequestParam(value = "car", required = false) String car,
                                       @RequestParam(value = "transaction_time", required = false) String transaction_time,
                                       @RequestParam(value = "money", required = false) String money,
                                       @RequestParam(value = "sum_money", required = false)String sum_money,
                                       @RequestParam(value = "count", required = false) String count) throws UnsupportedEncodingException {

        if (openidStr == null || user == null || station_name == null || car == null || transaction_time == null || money == null || sum_money == null || count == null) {
            throw new ParamNullException();
        }
        if (openidStr.equals("") || user.equals("") || station_name.equals("") || car.equals("") || transaction_time.equals("") || money.equals("") || sum_money.equals("") || count.equals("")) {
            throw new ParamNullException();
        }

        /**
         * server已经将GET的编码方式配成 UTF-8 故不需要此转换
         *
         * BUT server这样设置会导致log乱码,故又修改回来
         */
        station_name = new String(station_name.getBytes("iso8859-1"), "UTF-8");
        car = new String(car.getBytes("iso8859-1"), "UTF-8");

        //转换金额格式
        money = modelMsg.change2CNY(money);
//        sum_money = modelMsg.change2CNY(sum_money);
        sum_money = "￥" + sum_money;

        //转换时间格式
        transaction_time = transaction_time + "00";
        long timeStamp = Long.parseLong(transaction_time);
        transaction_time = modelMsg.changeDataFormat(timeStamp);

        /**
         * 从数据库中查询
         * off modelmg openid
         * 两天以前
         */
        Date date=new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -2);
        date = calendar.getTime();
        String two_days_before = sdf.format(date);
        ArrayList<String> user_off = transationDao.query_userful_openid(two_days_before);

        List<String> openidList = new ArrayList<>();

        for (String openid : openidStr.split(",")){
            if(openid.length() > 5 && !user_off.contains(openid)){
                openidList.add(openid);
            }
        }
//        /**
//         * 单独为胡老师的关闭推送
//         */
//        int status = transationDao.queryForModelMsgStatus("oNTV9wWIsxjMPmkG8AlIe8mAfNUo");
//        if(status == 1){
//            /**
//             * 设置单例modelmsgsource
//             * 与最新的modelmsgsource_new 比较 判定是否重复发送
//             * 加胡老师的openid，给每个油站都发送
//             */
//            ModelMsgScource modelMsgScource_new = new ModelMsgScource(user, station_name, transaction_time, money);
//            if(!modelMsgScource.equals(modelMsgScource_new)){
//                openidList.add("oNTV9wWIsxjMPmkG8AlIe8mAfNUo");
////                openidList.add("oNTV9wS72vrgzK9EI4-BGVIXJeM0");
//                modelMsgScource = modelMsgScource_new;
//            }
//        }
        Map<String, String> params = new HashMap<>();
        params.put("first", transaction_time);
        params.put("keyword1", station_name);
        params.put("keyword2", user);
        params.put("keyword3", car);
        params.put("keyword4", money);
        params.put("keyword5", sum_money);
        params.put("keyword6", count);

        ModelMsgReqSuccess respData = modelMsgSendService.getModelMsgSendStatus(params, openidList, ModelMsgStatus.TRANSATION.getIndex());
        return respData;
    }

    @ExceptionHandler(ParamNullException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ModelMsgReqFailure paramNullException() {
        ModelMsgReqFailure data = new ModelMsgReqFailure();
        data.setCode(1);
        data.setMsg("url param is invaild");
        return data;
    }

    @ExceptionHandler(NumberFormatException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ModelMsgReqFailure amountInvaild() {
        ModelMsgReqFailure data = new ModelMsgReqFailure();
        data.setCode(2);
        data.setMsg("amount or time_end param is invaild");
        return data;
    }

    /**
     * 给用户发模板消息的逻辑
     * @param ucouponMsg
     * @param request
     * @param response
     * @return
     * @throws UnsupportedEncodingException
     */
    @ResponseBody
    @RequestMapping(value = "modelMsg/to_user", method = RequestMethod.POST)
    public String sendModelMsgToUser(@RequestBody String ucouponMsg, HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {

        request.setCharacterEncoding("UTF-8");
        NumberFormat CHN = NumberFormat.getCurrencyInstance(Locale.CHINA);
        DecimalFormat df = new DecimalFormat("#.00");
        System.out.println("模板消息内容为：" + ucouponMsg);
        String code = null;
        JSONObject ucouponMsgObj = null;
        String out_trade_no = null;
        try {
            ucouponMsgObj = JSON.parseObject(ucouponMsg);
            code = ucouponMsgObj.getString("code");
            out_trade_no = ucouponMsgObj.getString("ordernum");
        } catch (Exception e) {
            code = "-1";
            System.out.println("模板消息内容异常");
        }
        if ("1".equals(code)) {

            System.out.println("----[14]----  ~~~~~~出票成功~~~~~~~~");
            logger.info("----[14]----  订单号： " + out_trade_no + "~~~~~出票成功~~~~");

            String stationName = new String (ucouponMsgObj.getString("stationname").getBytes("ISO-8859-1"), "UTF-8");//改变编码格式
            String passCode = ucouponMsgObj.getString("pass");
            String umoney = df.format(Double.parseDouble(ucouponMsgObj.getString("umoney")));
            umoney = CHN.format(Double.parseDouble(umoney));
            String ticket = ucouponMsgObj.getString("ticket");
            String openid = ucouponMsgObj.getString("openid");
            String amount_fee = ucouponMsgObj.getString("amount");
            //出票成功定制消息
            Map<String, String> ticket_success_msgs = new HashMap<>();
            ticket_success_msgs.put("openid", openid);
            ticket_success_msgs.put("first", "您好,出票成功,请前往加油\n");
            ticket_success_msgs.put("keyword1", out_trade_no);
            ticket_success_msgs.put("keyword2", stationName);
            ticket_success_msgs.put("keyword3", CHN.format(Double.parseDouble(amount_fee)));
            ticket_success_msgs.put("keyword4", passCode);

            StringBuffer remarkBuf = new StringBuffer();
            if (ticket.equals("1")) {
                remarkBuf.append("发票: 开具\n");
            } else {
                remarkBuf.append("发票: 不开具\n");
            }
            remarkBuf.append("\n本次加油返还U币: " + umoney + " \n\n请点击“获取APP”安装优加油APP，查看更多优惠券和返利");
            ticket_success_msgs.put("remark", remarkBuf.toString());

            String ticket_success_notify = modelMsg.sendTicketSuccessMsg(ticket_success_msgs);
            System.out.println("模板消息返回状态: " + ticket_success_notify);
            logger.info("----[14]----模板消息返回状态: " + ticket_success_notify);
            logger.info("----[14]----订单号： " + out_trade_no + "业务结束");
            System.out.println("\n" + out_trade_no + " finished");
            return "OK";
        } else {
            System.out.println("出票失败,退款逻辑");
            logger.error("----[14]----出票失败,退款逻辑");
//            int rowErrorOrder = transationDao.recordErrorOrder(out_trade_no, amount_fee, mobile);
//            System.out.println("订单 " + out_trade_no + "异常 记录 to Ucoupon status " + rowErrorOrder);
//            logger.error("订单 " + out_trade_no + "异常 记录 to Ucoupon status " + rowErrorOrder);
            return "ERROR";
        }
    }

}
