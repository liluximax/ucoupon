package com.weixin.pay.controller;

import com.alibaba.fastjson.JSON;
import com.weixin.pay.dao.TransationDao;
import com.weixin.pay.service.QRService;
import com.weixin.pay.service.WxPayService;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * Created by luc on 16/7/23.
 */
@Controller
@RequestMapping(value = "util")
public class Common {

    private static final String VALIDATE_NEW = "http://app1.u-coupon.cn:8000/new_wx/validate.php?mobile=MOBILE&authcode=CODE&station_id=STATION";

    private static final String DISCOUNT_CONPON = "http://app1.u-coupon.cn:8000/new_wx/promotionlist.php?username=MOBILE";

    private static final String QUERY_STATION = "http://103.235.227.186:8000/new_wx/query_station_name.php?station_id=STATION";

    private static final String GET_USER_UMONEY = "http://app1.u-coupon.cn:8000/new_wx/user_info.php?mobile=MOBILE";

    private static final String GET_STATION_BYSCAN = "http://app1.u-coupon.cn:8000/new_wx/scan_code.php?scan_code=SCAN_CODE";

    private static final String GET_STATION_BY_TGKEY= "http://app1.u-coupon.cn:8000/new_wx/weixin_code.php?scan_code=TG_KEY";

    private static final String GET_FAVORABLE = "https://app1.u-coupon.cn:448/Scaven/discount.php";

    @Autowired
    private WxPayService wxPayService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private QRService qrService;

    @Autowired
    private Logger logger;

    @Autowired
    private TransationDao transationDao;

    @RequestMapping(value = "station")
    public void getCityByReq(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        String lng = request.getParameter("lng");
        String lat = request.getParameter("lat");
        String openid = request.getParameter("openid");
        System.out.println("[" + openid + "] " + "get location: " + lng + "," + lat);
        logger.info("----[2]----定位openid:  [" + openid + "] " + " 位置信息: " + lng + "," + lat);

//        String stationList = wxPayService.getStationList(lng, lat);
        String stationList = wxPayService.getStationListByBaidu(lng, lat, openid);

        out.print(stationList);
    }

    @RequestMapping(value = "send_auth_code")
    public void sendAuthCode(@RequestParam("mobile") String mobile,
                             @RequestParam("callback") String callback,
                             HttpServletResponse response, HttpServletRequest request) throws IOException {
        PrintWriter out = response.getWriter();

        String referer = request.getHeader("referer");
        String codefromrefer = referer.substring(referer.indexOf("=") + 1, referer.indexOf("&"));

        HttpSession session = request.getSession();
        String codefromSession = (String) session.getAttribute("code");

        if (!codefromrefer.equals(codefromSession)){
            out.print("大哥，求放过啊！");
        }else{
            System.out.println("get referer: " + referer);
            logger.info("get tel: " + mobile);
            String result = wxPayService.sendAuthCode(mobile);
            System.out.println("----[3]----收到验证码json: " + result);
            logger.info("----[3]----收到验证码，返回数据: " + result);
            out.print(callback + "(" + result + ")");
        }

    }

    @RequestMapping(value = "validate_code")
    public void validateCode(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        String mobile = request.getParameter("mobile");
        String authcode = request.getParameter("auth_code");
        String station = request.getParameter("station");
        System.out.println("验证,验证码接口收到的数据: " + mobile + "," + authcode + "," + station);
        String result = wxPayService.validateCode(mobile, authcode, station);
        out.print(result);
    }

    @RequestMapping(value = "validate_code_new")
    @ResponseBody
    String validateCodeNew(@RequestParam(value = "mobile", required = false, defaultValue = "0") String mobile,
                           @RequestParam(value = "auth_code", required = false, defaultValue = "0") String authCode,
                           @RequestParam(value = "station", required = false, defaultValue = "0") String station) {

        String url = VALIDATE_NEW.replace("MOBILE", mobile)
                .replace("CODE", authCode)
                .replace("STATION", station);
        System.out.println(url);
        logger.info("----[4]----验证码验证URL: " + url);
        String result = restTemplate.getForObject(url, String.class);
        System.out.println(result + "\n");
        logger.info("----[4]----验证码验证接口，返回数据：" + result + "\n");
        return result;
    }

    @RequestMapping(value = "discount_coupon")
    @ResponseBody
    String discountCoupon(@RequestParam(value = "mobile", required = false, defaultValue = "0") String moblie) {
        String url = DISCOUNT_CONPON.replace("MOBILE", moblie);
        System.out.println(url);
        logger.info("----[5]----获取优惠券URL： " + url);
        long start = System.currentTimeMillis();
        String result = restTemplate.getForObject(url, String.class);
        logger.info("----[5]----获取优惠券接口，返回数据：" + result + "\n");
        long end = System.currentTimeMillis();
        logger.info("----[5]----获取优惠券用时: " + (end - start));
        return result;
    }


    @RequestMapping(value = "query_station_name", produces = "application/json;charset=utf-8")
    @ResponseBody
    String queryForStationNameById(@RequestParam(value = "station_id", required = false, defaultValue = "000") String stationId) {
        return JSON.toJSONString(qrService.queryForStationNameById(stationId));
    }

    @RequestMapping(value = "query_station_name_php")
    @ResponseBody
    String queryForStationNameByIdPhp(@RequestParam(value = "station_id", required = false, defaultValue = "000") String stationId) {
        String url = QUERY_STATION.replace("STATION", stationId);
        long start = System.currentTimeMillis();
        String result = restTemplate.getForObject(url, String.class);
        long end = System.currentTimeMillis();
        System.out.println("查询油站名字用时: " + (end - start));
        return result;
    }

    @RequestMapping(value = "get_user_Umoney")
    @ResponseBody
    String getUserUMoney(@RequestParam(value = "mobile") String mobile){
        String url = GET_USER_UMONEY.replace("MOBILE", mobile);
        logger.info("----[4]----获取用户U币URL: " + url);
        String result = restTemplate.getForObject(url, String.class);
        logger.info("----[4]----获取用户U币，返回数据：" + result + "\n");
        return result;
    }

    /**
     * 扫码获取油站
     * @param code_str
     * @return
     */
    @RequestMapping(value = "scan_station", produces = "application/json;charset=utf-8")
    @ResponseBody
    String getStationByScan(@RequestParam(value = "code_str") String code_str){
        String url = GET_STATION_BYSCAN.replace("SCAN_CODE", code_str);
        String result = restTemplate.getForObject(url, String.class);
        logger.info("-----[2X]-----扫码进行定位，返回数据：" + result + "\n");
        return result;

    }

    /**
     * 通过tg_key查询油站
     * @param eventKey
     * @return
     */
    @RequestMapping(value = "get_station_byEK", produces = "application/json;charset=utf-8")
    @ResponseBody
    String getStationBytgKey(@RequestParam(value = "eventKey") String eventKey){
        String url = GET_STATION_BY_TGKEY.replace("TG_KEY", eventKey);
        String result = restTemplate.getForObject(url, String.class);
        logger.info("-----[2X]-----通过eventKey查询定位，返回数据：" + result + "\n");
        return result;
    }

    public  String getIpAddr(HttpServletRequest request)  {
        String ip  =  request.getHeader( " x-forwarded-for " );
        if (ip  ==   null   ||  ip.length()  ==   0   ||   " unknown " .equalsIgnoreCase(ip))  {
            ip  =  request.getHeader( " Proxy-Client-IP " );
        }
        if (ip  ==   null   ||  ip.length()  ==   0   ||   " unknown " .equalsIgnoreCase(ip))  {
            ip  =  request.getHeader( " WL-Proxy-Client-IP " );
        }
        if (ip  ==   null   ||  ip.length()  ==   0   ||   " unknown " .equalsIgnoreCase(ip))  {
            ip  =  request.getRemoteAddr();
        }
        return  ip;
    }

    /**
     * 临时的
     * 获取优惠
     * @param request
     * @param response
     */
    @ResponseBody
    @RequestMapping(value = "fetch_favorable")
    public String getFavorable(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        data = new String(data.getBytes("iso8859-1"), "utf-8");
        data = URLDecoder.decode(data, "utf-8");
        int index = data.indexOf("=");
        if (index != -1)
            data = data.substring(0, data.indexOf("="));
        System.out.println(data);
        String result = restTemplate.postForObject(GET_FAVORABLE, data, String.class);
        System.out.println("result:" + result);

        return  result;
    }
}
