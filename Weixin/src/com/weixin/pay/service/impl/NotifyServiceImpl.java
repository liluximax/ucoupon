package com.weixin.pay.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.test.WeixinTest;
import com.weixin.pay.dao.TransationDao;
import com.weixin.pay.domain.generateOrder.OrderData;
import com.weixin.pay.domain.modelMsg.ModelMsgInfo;
import com.weixin.pay.domain.modelMsg.ModelMsgReqSuccess;
import com.weixin.pay.domain.modelMsg.ModelMsgResp;
import com.weixin.pay.domain.modelMsg.ModelMsgStatus;
import com.weixin.pay.service.ModelMsg;
import com.weixin.pay.service.ModelMsgSendService;
import com.weixin.pay.service.NotifyService;
import com.weixin.pay.service.WxPayService;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

/**
 * Created by luc on 17/1/16.
 */
@Service
public class NotifyServiceImpl implements NotifyService, ModelMsgSendService {


    @Autowired
    private Logger logger;

    @Autowired
    private ModelMsg modelMsg;

    @Autowired
    private WxPayService wxPayService;

    @Autowired
    private TransationDao transationDao;

    @Override
    public void notifyUcouponOnFinished(String openid, String out_trade_no, String time_end, double amount_fee, String mobile, String stationId, OrderData orderData) {

        NumberFormat CHN = NumberFormat.getCurrencyInstance(Locale.CHINA);
        DecimalFormat df = new DecimalFormat("#.00");

        /**
         * 通知订单消息至ucoupon后台
         */
        String ucouponMsg = wxPayService.sendWxOrder(out_trade_no, orderData);
        String code = new String();
        JSONObject ucouponMsgObj = null;
        try {
            ucouponMsgObj = JSON.parseObject(ucouponMsg);
            code = ucouponMsgObj.getString("code");
        } catch (Exception e) {
            code = "-1";
            System.out.println(out_trade_no + "通知ucoupon后台出现异常");
            logger.error("----[13]----订单号： " + out_trade_no + "通知ucoupon后台出现异常" );
        }

        if ("1".equals(code)) {

            System.out.println("----[14]----  ~~~~~~出票成功~~~~~~~~");
            logger.info("----[14]----  订单号： " + out_trade_no + "~~~~~出票成功~~~~");
            String stationName = ucouponMsgObj.getString("stationname");

            String passCode = ucouponMsgObj.getString("pass");

            String umoney = df.format(Double.parseDouble(ucouponMsgObj.getString("umoney")));
            umoney = CHN.format(Double.parseDouble(umoney));

            String ticket = ucouponMsgObj.getString("ticket");

            //出票成功定制消息
            Map<String, String> ticket_success_msgs = new HashMap<>();
            ticket_success_msgs.put("openid", openid);
            ticket_success_msgs.put("first", "您好,出票成功,请前往加油\n");
            ticket_success_msgs.put("keyword1", out_trade_no);
            ticket_success_msgs.put("keyword2", stationName);
            ticket_success_msgs.put("keyword3", CHN.format(amount_fee));
            ticket_success_msgs.put("keyword4", passCode);

            StringBuffer remarkBuf = new StringBuffer();
            if (ticket.equals("1")) {
                remarkBuf.append("发票: 开具\n");
            } else {
                remarkBuf.append("发票: 不开具\n");
            }
            remarkBuf.append("\n本次加油返还U币: " + umoney + " \n\n请点击“获取APP”安装优加油APP，查看更多优惠券和返利");
            ticket_success_msgs.put("remark", remarkBuf.toString());

            String ticket_success_notify = modelMsg.sendTicketSuccessMsg(ticket_success_msgs);
            System.out.println("模板消息返回状态: " + ticket_success_notify);
            logger.info("----[14]----模板消息返回状态: " + ticket_success_notify);
            logger.info("----[14]----订单号： " + out_trade_no + "业务结束");
            System.out.println("\n" + out_trade_no + " finished");

        } else {
            System.out.println("出票失败,退款逻辑");


            logger.error("----[14]----出票失败,退款逻辑");

            /**
             * 异常情况,暂时不发模板消息
             */
//            String stationName = wxPayService.fetchStationNameById(stationId);
//
//            //发送出票失败模板消息
//            Map<String, String> ticket_failure_msgs = new HashMap<>();
//            ticket_failure_msgs.put("openid", openid);
//            ticket_failure_msgs.put("first", "您好,凭证出票失败\n");
//            ticket_failure_msgs.put("keyword1", out_trade_no);
//            ticket_failure_msgs.put("keyword2", stationName);
//            ticket_failure_msgs.put("keyword3", CHN.format(amount_fee));
//            ticket_failure_msgs.put("remark", "\n优加油对于不便表示歉意,支付金额将在1至2个工作日内原路退回您的账户");
//
//            String ticket_failure_notify = modelMsg.sendTicketFailureMsg(ticket_failure_msgs);
//            System.out.println("模板消息返回状态: " + ticket_failure_notify);

            System.out.println("\n" + out_trade_no + " finished");
            logger.info("----[14]----订单号： " + out_trade_no + "业务结束");
            int rowErrorOrder = transationDao.recordErrorOrder(out_trade_no, amount_fee, mobile);
            System.out.println("订单 " + out_trade_no + "异常 记录 to Ucoupon status " + rowErrorOrder);

            logger.error("订单 " + out_trade_no + "异常 记录 to Ucoupon status " + rowErrorOrder);
        }

        //插入数据库
        int row2Coupon = transationDao.recordTransation2Ucoupon(out_trade_no, orderData, time_end);
        System.out.println("订单 " + out_trade_no + " 记录 to Ucoupon status " + row2Coupon);
    }



    @Override
    public ModelMsgReqSuccess getModelMsgSendStatus(Map<String, String> param, List<String> users, int status) {

        ModelMsgReqSuccess respData = new ModelMsgReqSuccess();
        List<ModelMsgInfo> modelMsgInfos = new ArrayList<>();
        int msgSuccess = 0, msgFailure = 0;

        respData.setMsg_num(users.size());
        /**
         * 给每个openid发送消息
         */
        for (String openid : users) {

            ModelMsgResp modelMsgResp = new ModelMsgResp();
            String modelMsgResp_pre = new String();
            if (status == ModelMsgStatus.DELAY.getIndex()) {
                Map<String, String> map = generateDelayMsg(param, openid);
                modelMsgResp_pre = modelMsg.sendTicketDelayMsg(map);
                modelMsgResp = JSON.parseObject(modelMsgResp_pre, ModelMsgResp.class);
            }
            else if(status == ModelMsgStatus.TRANSATION.getIndex()){
                //TODO
                Map<String, String> map = generateTransactionMsg(param, openid);
                modelMsgResp_pre = modelMsg.sendTransSuccessNotify(map);
                modelMsgResp = JSON.parseObject(modelMsgResp_pre, ModelMsgResp.class);
            }

            String original_Time = param.get("first").replaceAll("-", "").replaceAll(" ", "").replaceAll(":", "");

            original_Time = original_Time.substring(0, original_Time.length() - 2);

//            System.out.println("original_Time : " + original_Time);

            long insert_time = System.currentTimeMillis();

            int row2Coupon = transationDao.recordModelMsgStatus(status, openid, param, modelMsgResp_pre, original_Time, insert_time);
            System.out.println("发送模板消息到 " + openid + " 记录 to Ucoupon status " + row2Coupon );

            System.out.println("模板消息发送状态: " + modelMsgResp);
            int code = modelMsgResp.getErrcode();
            if (code == 0)
                msgSuccess++;
            else
                msgFailure++;

            ModelMsgInfo modelMsgInfo = new ModelMsgInfo(modelMsgResp, openid);
            modelMsgInfos.add(modelMsgInfo);
        }


        respData.setCode(0);
        respData.setMsg("param is valid");
        respData.setMsg_success_num(msgSuccess);
        respData.setMsg_failure_num(msgFailure);
        respData.setMsg_detail(modelMsgInfos);

        return respData;
    }

    public Map<String, String> generateDelayMsg(Map<String, String> param, String openid) {
        Map<String, String> map = new HashMap<>();
        map.put("first", "您好, " + param.getOrDefault("first", "") + " 有一笔交易,由于网络原因出票延迟\n");
        map.put("keyword1", param.getOrDefault("keyword1", ""));
        map.put("keyword2", param.getOrDefault("keyword2", ""));
        map.put("keyword3", param.getOrDefault("keyword3", ""));
        map.put("keyword4", param.getOrDefault("keyword4", ""));
        map.put("keyword5", param.getOrDefault("keyword5", ""));
        map.put("remark", "\n网络恢复会补打票据,对于不便深表歉意");
        map.put("openid", openid);
        return map;
    }

    public Map<String, String> generateTransactionMsg(Map<String, String> param, String openid) {
        Map<String, String> map = new HashMap<>();
        map.put("first", "您好, " + param.getOrDefault("first", "") + " 发生了一笔油品交易\n" +
                "今天共" + param.getOrDefault("keyword6", "") + "笔交易, " +
                "交易总额: " + param.getOrDefault("keyword5", "") + "元\n");
        map.put("keyword1", param.getOrDefault("keyword1", ""));
        map.put("keyword2", param.getOrDefault("keyword2", ""));
        map.put("keyword3", param.getOrDefault("keyword3", ""));
        map.put("keyword4", param.getOrDefault("keyword4", ""));
        map.put("remark", "\n请留意出票情况，回复“3”可开启/关闭消息推送");
        map.put("openid", openid);
        return map;
    }

}
