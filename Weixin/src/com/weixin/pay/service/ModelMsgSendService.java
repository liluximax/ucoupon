package com.weixin.pay.service;

import com.weixin.pay.domain.modelMsg.ModelMsgReqSuccess;

import java.util.List;
import java.util.Map;

/**
 * Created by luc on 17/4/13.
 */
public interface ModelMsgSendService {
    //获取发送模板消息返回状态
    ModelMsgReqSuccess getModelMsgSendStatus(Map<String, String> param, List<String> users, int status);
}
