package com.weixin.pay.service;

import com.weixin.pay.domain.generateOrder.OrderData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2018/01/11 0011.
 */
@Service
public class RedisService {

    @Autowired
    private RedisTemplate redisTemplate;


    @Cacheable(value = "orderData", key = "#out_trade_no")
    public OrderData handleByRedisCache(String out_trade_no, OrderData orderData){
        System.out.println("查询订单信息");
        return orderData;
    }

//    public OrderData getOrderDataFromRedis(String out_trade_no){
//        OrderData orderData = (OrderData) redisTemplate.opsForValue().get(out_trade_no);
//        System.out.println("从redis 数据库中进行查询...");
//        return orderData;
//
//    }
//
//    public void addOrderDatatoRedis(String out_trade_no, OrderData orderData){
//        try{
//            redisTemplate.opsForValue().set(out_trade_no, orderData);
//            System.out.println("添加到Redis数据库当中...");
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }

}
