package com.weixin.pay.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * Created by luc on 17/1/10.
 */
public interface AdminService {


    /**
     * 获取今日订单（JSON格式）
     * @param openid
     * @return
     */
    JSONObject queryTodayOrdersInfo(String openid);

    /**
     * 获取今日订单总量
     * @param json
     * @return
     */
    int getTodayOrdersSize(JSONObject json);

    /**
     * 返回几日查询订单信息
     * @param openid
     * @param createTime
     * @return
     */
//    String queryTodayOrders(JSONObject json, String createTime, int n);
    String queryTodayOrders(String openid, String createTime);

    /**
     * 绑定openid和手机号码
     * @param openid
     * @param mobile
     * @param code
     * @return
     */
    String linkOpenidWithMobile(String openid, String mobile, String code);

    /**
     * BDYZ返回当前openid绑定油站信息
     * @param openid
     * @return
     */
    String queryBDStation(String openid);

    /**
     * 通过openid获取用户的相关信息
     * 太耗时,暂不使用
     * @param openid
     * @return
     */
    String fetchNickNameByOpenid(String openid);


    /**
     * writeby hl
     * 2017/05/09
     * 关闭消息提醒
     * @param openid
     * @return
     */
     String turn_on_or_off_Notification(String openid);

    /**
     * writeby hl
     * 2017/05/11
     * @param openid
     * @param startTime
     * @param endTime
     * @return
     */
//    String QueryOrders(String openid, Long startTime, Long endTime);
}
