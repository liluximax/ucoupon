package com.weixin.pay.dao;

import com.weixin.pay.domain.StationInfo;
import com.weixin.pay.domain.StationList;
import com.weixin.pay.domain.generateOrder.OrderData;
import com.weixin.pay.domain.generateOrder.OrderStatus;
import com.weixin.pay.domain.modelMsg.ModelMsgStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by luc on 16/8/3.
 */
@Repository
public class TransationDao {

    @Autowired
    @Qualifier("ucouponnamedjdbc")
    private NamedParameterJdbcTemplate namedParameterJdbcTemplateUcoupon;

    @Autowired
    @Qualifier("ucouponjdbc")
    private JdbcTemplate jdbcTemplateUcoupon;

    private static String sql = "insert into weixin_db.pay_tb (trade, amount, fee, umoney, coupon, duration, mobile, license, station_id, transation_date)"
            + " values (:t,:a,:f,:u,:c,:dr,:m,:l,:s,:d)";

    private static final String STATUS_SQL = "INSERT INTO weixin_db.pay_status (trade_no, status) VALUES (:t, :s) ";

    private static final String QUERY_STATION = " SELECT station_name FROM weixin_db.station_info WHERE station_id = ? ";

    private static final String QUERY_STATION_UCOUPON = " SELECT station_name FROM ucoupon.station_info WHERE station_id = ? ";

    private static final String NOTIFY_COUPON_URL = " INSERT INTO wx_front_end.trasation_url_tb (out_trade_no, mobile, url_log) VALUE (?,?,?)";

    private static String INSERT_TRASATION_COUPON = " insert into wx_front_end.trasation_tb (trade, amount, fee, umoney, coupon, duration, mobile, license, station_id, transation_date)"
            + " values (:t,:a,:f,:u,:c,:dr,:m,:l,:s,:d)";

    private static String RECORD_UNIFIEDORDER = "INSERT INTO wx_front_end.wx_order(out_trade_no, pay_status, pay_action, openid, amount, fee, umoney, coupon, coupon_list, duration, mobile, license, station_id, station_name, create_time)" +
            " VALUES (:out_trade_no, :pay_status, :pay_action, :openid, :amount, :fee, :umoney, :coupon, :coupon_list, :duration, :mobile, :license, :station_id, :station_name, :create_time)";

    private static String RECORD_WX_UNIFIEDORDER = "UPDATE wx_front_end.wx_order SET pay_status = :pay_status, pay_action = :pay_action, duration = :duration WHERE out_trade_no = :out_trade_no";

    private static String RECORD_TRADE_FINISHED = "UPDATE wx_front_end.wx_order SET transaction_id = :transaction_id, pay_status = :pay_status, pay_action = :pay_action, finish_time = :finish_time WHERE out_trade_no = :out_trade_no";

    private static String RECORD_ERROR_ORDER = "INSERT INTO wx_front_end.order_error (out_trade_no, amount, mobile) VALUES (:o, :a, :m)";

    private static String RECORD_LOCATE_SQL = "INSERT INTO wx_front_end.locate_log (openid, lng, lat, min, station_id, station_name) VALUES (:o, :lng, :lat, :m, :id, :name)";

    private static  String MODELMSG_STATUS = "INSERT INTO wx_front_end.modelmsg_tb_new(openid, user_mobile, out_trade_no, station_name, car, original_time, amount, modelmsg_resp, msg_type, insert_time)" +
            "VALUES(:openid, :user_mobile, :out_trade_no, :station_name, :car, :original_time, :amount, :modelmsg_resp, :msg_type, :insert_time)";

    private static String RECORDING_OPENID_MODELMSG ="INSERT INTO wx_front_end.on_off_modelmsg (openid, status, update_time) VALUES(:o, :s, :u)";

    private static String RECORDING_ON_OFF_MODELMSG = "UPDATE wx_front_end.on_off_modelmsg SET status = ABS(status -1), update_time = :update_time WHERE openid = :openid";

    private static String QUERY_MODELMSG_STATUS = "SELECT status from wx_front_end.on_off_modelmsg WHERE openid = ?";

    private static String QUERY_MODELMSG_OPENID_ISEXIST = "SELECT count(*) from wx_front_end.on_off_modelmsg WHERE openid = ?";

    private static String QUERY_USERFUL_OPENID_MSG = "SELECT openid from wx_front_end.on_off_modelmsg WHERE update_time > ? AND status = \'0\'";


    private static SimpleDateFormat timeStamp =  new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );

    /**
     * 记录订单FetchOutTradeNo状态
     * @param out_trade_no
     * @param couponList
     * @param orderData
     * @return
     */
    public int recordUnifiedorder(String out_trade_no, String couponList, OrderData orderData){
        try {
            MapSqlParameterSource source = new MapSqlParameterSource()
                    .addValue("out_trade_no", out_trade_no)
                    .addValue("pay_status", OrderStatus.FetchOutTradeNo.getStatus())
                    .addValue("pay_action", OrderStatus.FetchOutTradeNo.getAction())
                    .addValue("openid", orderData.getOpenid())
                    .addValue("amount", orderData.getAmount())
                    .addValue("fee", String.valueOf(Double.valueOf(orderData.getFee()) / 100))
                    .addValue("umoney", orderData.getUmoney_discount())
                    .addValue("coupon", orderData.getCoupon_discount())
                    .addValue("coupon_list", couponList)
                    .addValue("duration", String.valueOf(orderData.getDuration()))
                    .addValue("mobile", orderData.getTel())
                    .addValue("license", orderData.getLicense())
                    .addValue("station_id", orderData.getStation_id())
                    .addValue("station_name", orderData.getStation_name())
                    .addValue("create_time", timeStamp.format(orderData.getCreateTime()));
            return namedParameterJdbcTemplateUcoupon.update(RECORD_UNIFIEDORDER, source);
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 记录订单CreateTrade状态
     * @param out_trade_no
     * @return
     */
    public int recordUnifiedorder(String out_trade_no, long duration){
        try {
            MapSqlParameterSource source = new MapSqlParameterSource()
                    .addValue("out_trade_no", out_trade_no)
                    .addValue("pay_status", OrderStatus.CreateTrade.getStatus())
                    .addValue("pay_action", OrderStatus.CreateTrade.getAction())
                    .addValue("duration", duration);
            return namedParameterJdbcTemplateUcoupon.update(RECORD_WX_UNIFIEDORDER, source);
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 记录订单PayFinished状态
     * @param out_trade_no
     * @param transation_id
     * @param time_end
     * @return
     */
    public int recordeTradeFinished(String out_trade_no, String transation_id, String time_end){
        try {
            MapSqlParameterSource source = new MapSqlParameterSource()
                    .addValue("out_trade_no", out_trade_no)
                    .addValue("transaction_id", transation_id)
                    .addValue("pay_status", OrderStatus.PayFinished.getStatus())
                    .addValue("pay_action", OrderStatus.PayFinished.getAction())
                    .addValue("finish_time", time_end);
            return namedParameterJdbcTemplateUcoupon.update(RECORD_TRADE_FINISHED, source);
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }
    /**
     * 记录订单PayFinishedJS状态
     * @param out_trade_no
     * @param transation_id
     * @param time_end
     * @return
     */
    public int recordeTradeFinishedJS(String out_trade_no, String transation_id, String time_end){
        try {
            MapSqlParameterSource source = new MapSqlParameterSource()
                    .addValue("out_trade_no", out_trade_no)
                    .addValue("transaction_id", transation_id)
                    .addValue("pay_status", OrderStatus.PayFinishedJS.getStatus())
                    .addValue("pay_action", OrderStatus.PayFinishedJS.getAction())
                    .addValue("finish_time", time_end);
            return namedParameterJdbcTemplateUcoupon.update(RECORD_TRADE_FINISHED, source);
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    public int recordTransation2Ucoupon(String out_trade_no, OrderData orderData, String time_end) {
        try {
            MapSqlParameterSource source = new MapSqlParameterSource()
                    .addValue("t", out_trade_no)
                    .addValue("a", orderData.getAmount())
                    .addValue("f", String.valueOf(Double.valueOf(orderData.getFee()) / 100))
                    .addValue("u", orderData.getUmoney_discount())
                    .addValue("c", orderData.getCoupon_discount())
                    .addValue("dr", String.valueOf(orderData.getDuration()))
                    .addValue("m", orderData.getTel())
                    .addValue("l", orderData.getLicense())
                    .addValue("s", orderData.getStation_id())
                    .addValue("d", time_end);
            return namedParameterJdbcTemplateUcoupon.update(INSERT_TRASATION_COUPON, source);
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 记录出票失败的订单
     * 包括ucoupon后台异常
     * @param out_trade_no
     * @param amount
     * @param mobile
     * @return
     */
    public int recordErrorOrder(String out_trade_no, double amount, String mobile){
        try {
            MapSqlParameterSource source = new MapSqlParameterSource().addValue("o", out_trade_no)
                    .addValue("a", amount)
                    .addValue("m", mobile);
            return namedParameterJdbcTemplateUcoupon.update(RECORD_ERROR_ORDER, source);
        }catch (Exception e){
            return 0;
        }

    }

    public String queryForStationNameByIdFromUcoupon(String stationId){
        try {
            return jdbcTemplateUcoupon.queryForObject(QUERY_STATION_UCOUPON, new Object[]{stationId}, String.class);
        }catch (Exception e){
            return "NA";
        }
    }

    /**
     * 记录支付成功后 通知ucoupon的url
     * @param out_trade_no
     * @param mobile
     * @param url
     * @return
     */
    public int recordUcouponNotify(String out_trade_no, String mobile, String url){
        try {
            return jdbcTemplateUcoupon.update(NOTIFY_COUPON_URL, out_trade_no, mobile, url);
        }catch (Exception e){
            return 0;
        }
    }

    /**
     * 记录每次定位结果
     * @param lng
     * @param lat
     * @param openid
     * @param stationList
     * @return
     */
    public int recordLocateLog(String lng, String lat, String openid, StationList stationList){
        MapSqlParameterSource source = new MapSqlParameterSource();
        List<StationInfo> list = stationList.getList();
        if(list != null)
           source.addValue("o", openid)
                   .addValue("lng", lng)
                   .addValue("lat", lat)
                   .addValue("m", list.get(0).getMin())
                   .addValue("id", list.get(0).getStation_id())
                   .addValue("name", list.get(0).getStation_name());
        else
            source.addValue("o", openid)
                    .addValue("lng", lng)
                    .addValue("lat", lat)
                    .addValue("m", -1)
                    .addValue("id", "NA")
                    .addValue("name", "NA");
        try {
            return namedParameterJdbcTemplateUcoupon.update(RECORD_LOCATE_SQL, source);
        }catch (Exception e){
            return 0;
        }
    }


    /**
     * 记录每次给openID发送模板消息
     */
    public int recordModelMsgStatus(int status,String openid, Map<String, String> map, String modelmsg_resp, String original_Time, long insert_Time){
        try {
            MapSqlParameterSource source = new MapSqlParameterSource();
            if (status == ModelMsgStatus.DELAY.getIndex()){
                source
                        .addValue("openid", openid)
                        .addValue("user_mobile", map.get("keyword4"))
                        .addValue("out_trade_no", map.get("keyword1"))
                        .addValue("station_name", map.get("keyword2"))
                        .addValue("car",          map.get("keyword5"))
                        .addValue("original_time", original_Time)
                        .addValue("amount",      map.get("keyword3"))
                        .addValue("modelmsg_resp", modelmsg_resp)
                        .addValue("msg_type", status)
                        .addValue("insert_time", timeStamp.format(insert_Time));
            }else {
               source
                        .addValue("openid", openid)
                        .addValue("user_mobile", map.get("keyword2"))
                        .addValue("out_trade_no", "NA")
                        .addValue("station_name", map.get("keyword1"))
                        .addValue("car",          map.get("keyword3"))
                        .addValue("original_time", original_Time)
                        .addValue("amount",      map.get("keyword4"))
                        .addValue("modelmsg_resp", modelmsg_resp)
                        .addValue("msg_type", status)
                        .addValue("insert_time", timeStamp.format(insert_Time));
            }

            return namedParameterJdbcTemplateUcoupon.update(MODELMSG_STATUS, source);

        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 查询对应openid的记录 是否存在
     * @param openid
     * @return
     */
    public int queryForModelMsgOpenidIsExist(String openid){
        try{
            Integer result = jdbcTemplateUcoupon.queryForObject(QUERY_MODELMSG_OPENID_ISEXIST, new Object[]{openid}, Integer.class);
            return result;
        }catch (Exception e){
            e.printStackTrace();
            return -1;
        }

    }

    /**
     *查询对应openid的 status
     * @param openid
     * @return
     */
    public int queryForModelMsgStatus(String openid){
        try{
            Integer status = jdbcTemplateUcoupon.queryForObject(QUERY_MODELMSG_STATUS, new Object[]{openid}, Integer.class);
            return status;
        }catch (Exception e){
            return  -1;
        }
    }

    /**
     * 将开启关闭模板消息的openid 至数据库
     * 并将status设置0 ，关闭模板消息推送
     * @param openid
     * @param update_time
     * @return
     */
    public int recording_openid_modelMsg(String openid, long update_time){
        try{
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource().addValue("o", openid).addValue("s", 0).addValue("u", timeStamp.format(update_time));
            return namedParameterJdbcTemplateUcoupon.update(RECORDING_OPENID_MODELMSG, mapSqlParameterSource);
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 设置openid的模板消息推送开启/关闭状态
     * @param openid
     * @param update_time
     * @return
     */
    public int recording_on_off_modelMsg(String openid, long update_time){
        try{
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource()
                    .addValue("update_time", timeStamp.format(update_time))
                    .addValue("openid", openid);
            return  namedParameterJdbcTemplateUcoupon.update(RECORDING_ON_OFF_MODELMSG, mapSqlParameterSource);

        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 从msg 中
     * 返回有效的openid
     * @param today
     * @return
     */
    public ArrayList<String> query_userful_openid(String today){
        try{
            final ArrayList<String> list = new ArrayList<>();
            jdbcTemplateUcoupon.query(QUERY_USERFUL_OPENID_MSG, new Object[]{today}, new RowCallbackHandler() {

                @Override
                public void processRow(ResultSet rs) throws SQLException {
                    String openid = rs.getString("openid");
                    list.add(openid);
                }
            }
            );

            return list;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }


}
