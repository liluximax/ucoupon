package com.weixin.pay.domain.message.transcationNotify;

import com.weixin.pay.domain.message.Template;

/**
 * Created by luc on 17/4/13.
 * 交易成功通知加油员模板消息
 *
 * {{first.DATA}}
 * 油站名称：{{keyword1.DATA}}
 * 手机号码：{{keyword2.DATA}}
 * 车牌号码：{{keyword3.DATA}}
 * 交易金额：{{keyword4.DATA}}
 * {{remark.DATA}}
 */
public class TemplateTranscationSuccess extends Template{
    private TranscationSuccess data;

    public TranscationSuccess getData() {
        return data;
    }

    public void setData(TranscationSuccess data) {
        this.data = data;
    }

    public TemplateTranscationSuccess(TranscationSuccess data) {

        this.data = data;

        super.setTemplate_id("zrs5jLm-hQe8Y-EJRyP8C7xoqcQZKlSna2hbtAaHgfc");

    }
}
