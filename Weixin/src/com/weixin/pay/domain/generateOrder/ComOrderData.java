package com.weixin.pay.domain.generateOrder;

import com.alibaba.fastjson.annotation.JSONType;

import java.io.Serializable;
import java.util.List;

/**
 * Created by luc on 16/10/16.
 */
@JSONType(orders = {"openid", "createTime", "fee", "amount", "favorable", "duration", "umoney_discount", "coupon_discount", "tel", "license", "station_id", "station_name", "ticket", "coupon_list"})
public class ComOrderData extends OrderData  {

    private String tel;
    private String license;
    private String ticket;
    private String umoney_discount;
    private String coupon_discount;
    private List<String> coupon_list;
    public String distinguish = "COMORDERDATA";

    public ComOrderData(String amount, String coupon_discount, List<String> coupon_list, Long createTime, Long duration, String favorable, String fee, String license, String openid, String station_id, String station_name, String tel, String ticket, String umoney_discount) {
        super(amount,createTime,favorable, fee, openid,station_id,station_name);
        this.tel = tel;
        this.license = license;
        this.coupon_discount= coupon_discount;
        this.umoney_discount = umoney_discount;
        this.ticket = ticket;
        this.coupon_list = coupon_list;
    }

    public ComOrderData() {
    }

    @Override
    public String toString() {
        return "OrderData{" +
                "amount='" + amount + '\'' +
                ", openid='" + openid + '\'' +
                ", createTime=" + createTime +
                ", fee='" + fee + '\'' +
                ", tel='" + tel + '\'' +
                ", license='" + license + '\'' +
                ", station_id='" + station_id + '\'' +
                ", station_name='" + station_name + '\'' +
                ", ticket='" + ticket + '\'' +
                ", favorable='" + favorable + '\'' +
                ", umoney_discount='" + umoney_discount + '\'' +
                ", coupon_discount='" + coupon_discount + '\'' +
                ", duration=" + duration +
                ", coupon_list=" + coupon_list +
                '}';
    }

    public String getCoupon_discount() {
        return coupon_discount;
    }

    public void setCoupon_discount(String coupon_discount) {
        this.coupon_discount = coupon_discount;
    }

    public List<String> getCoupon_list() {
        return coupon_list;
    }

    public void setCoupon_list(List<String> coupon_list) {
        this.coupon_list = coupon_list;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getUmoney_discount() {
        return umoney_discount;
    }

    public void setUmoney_discount(String umoney_discount) {
        this.umoney_discount = umoney_discount;
    }

    @Override
    public String getDistinguish() {
        return distinguish;
    }

    public void setDistinguish(String distinguish) {
        this.distinguish = distinguish;
    }
}

