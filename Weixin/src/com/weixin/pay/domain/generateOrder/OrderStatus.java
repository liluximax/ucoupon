package com.weixin.pay.domain.generateOrder;

/**
 * Created by luc on 17/9/13.
 */
public enum OrderStatus {

    FetchOutTradeNo("FETCH_OUT_TRADE_NO", 1),   //向Ucoupon获取out_trade_no状态
    CreateTrade("CREATE_TRADE", 2), //微信统一下单状态
    PayFinished("PAY_FINISHED", 3), //订单成功支付状态
    PayFinishedJS("PAY_FINISHED_JS", 4);    //订单成功支付但是通过JS通知状态

    private int status;
    private String action;

    OrderStatus(String action, int status) {
        this.action = action;
        this.status = status;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
