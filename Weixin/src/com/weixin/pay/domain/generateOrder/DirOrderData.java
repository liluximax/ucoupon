package com.weixin.pay.domain.generateOrder;

import com.alibaba.fastjson.annotation.JSONType;

import java.io.Serializable;
import java.util.List;

@JSONType
public class DirOrderData extends OrderData{

    private String out_trade_no;

    public String distinguish = "DIRORDERDATA";

    public DirOrderData(String amount,Long createTime,String favorable, String fee,String openid, String station_id, String station_name, String out_trade_no){
        super(amount,createTime,favorable, fee, openid,station_id,station_name);
        this.out_trade_no = out_trade_no;
    }

    public DirOrderData(){}


    @Override
    public String toString() {
        return "OrderData{" +
                "amount='" + amount + '\'' +
                ", openid='" + openid + '\'' +
                ", createTime=" + createTime +
                ", fee='" + fee + '\'' +
                ", station_id='" + station_id + '\'' +
                ", station_name='" + station_name + '\'' +
                ", favorable='" + favorable + '\'' +
                ", duration=" + duration +
                '}';
    }

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    @Override
    public String getDistinguish() {
        return distinguish;
    }

    public void setDistinguish(String distinguish) {
        this.distinguish = distinguish;
    }
}
