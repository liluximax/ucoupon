package com.weixin.pay.domain.generateOrder;

import com.alibaba.fastjson.annotation.JSONType;

import java.io.Serializable;
import java.util.List;

/**
 * Created by luc on 16/10/16.
 */
@JSONType
public class OrderData implements Serializable{

    public String openid;
    public Long createTime;
    public String fee;
    public String station_id;
    public String station_name;
    public String amount;
    public String favorable;
    public Long duration = 0l;

    public String distinguish = "COMORDERDATA";

    public String getDistinguish() {
        return distinguish;
    }

    public void setDistinguish(String distinguish) {
        this.distinguish = distinguish;
    }

    public OrderData() {
    }
    public OrderData(String amount,Long createTime,String favorable, String fee,String openid, String station_id, String station_name){
        this.amount = amount;
        this.openid = openid;
        this.station_id = station_id;
        this.station_name = station_name;
        this.fee = fee;
        this.createTime = createTime;
        this.favorable = favorable;

    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCoupon_discount() {
        return "0";
    }

    public void setCoupon_discount(String coupon_discount) {

    }

    public List<String> getCoupon_list() {
        return null;
    }

    public void setCoupon_list(List<String> coupon_list) {

    }

    public String getFavorable() {
        return favorable;
    }

    public void setFavorable(String favorable) {
        this.favorable = favorable;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getLicense() {
        return "0000";
    }

    public void setLicense(String license) {

    }

    public String getTicket() {
        return "0000";
    }

    public void setTicket(String ticket) {

    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getStation_id() {
        return station_id;
    }

    public void setStation_id(String station_id) {
        this.station_id = station_id;
    }

    public String getStation_name() {
        return station_name;
    }

    public void setStation_name(String station_name) {
        this.station_name = station_name;
    }

    public String getTel() {
        return "00000000000";
    }

    public void setTel(String tel) {

    }

    public String getUmoney_discount() {
        return "0";
    }

    public void setUmoney_discount(String umoney_discount) {

    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getOut_trade_no() {
        return null;
    }

    public void setOut_trade_no(String out_trade_no) {
    }

}
