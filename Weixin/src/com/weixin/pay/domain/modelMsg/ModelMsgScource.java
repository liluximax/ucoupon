package com.weixin.pay.domain.modelMsg;

import java.util.Objects;

/**
 * Created by Administrator on 2017/12/29 0029.
 */
public class ModelMsgScource {

    String user;
    String station_name;
    String transaction_time;
    String money;

    public ModelMsgScource( String user, String station_name, String transaction_time, String money) {
        this.user = user;
        this.station_name = station_name;
        this.transaction_time = transaction_time;
        this.money = money;
    }

    public ModelMsgScource(){
        this.user = "NA";
        this.station_name = "NA";
        this.transaction_time = "NA";
        this.money = "NA";
    }
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getStation_name() {
        return station_name;
    }

    public void setStation_name(String station_name) {
        this.station_name = station_name;
    }

    public String getTransaction_time() {
        return transaction_time;
    }

    public void setTransaction_time(String transaction_time) {
        this.transaction_time = transaction_time;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModelMsgScource that = (ModelMsgScource) o;
        return Objects.equals(user, that.user) &&
                Objects.equals(station_name, that.station_name) &&
                Objects.equals(transaction_time, that.transaction_time) &&
                Objects.equals(money, that.money);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, station_name, transaction_time, money);
    }
}
