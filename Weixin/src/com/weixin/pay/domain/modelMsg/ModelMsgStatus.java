package com.weixin.pay.domain.modelMsg;

/**
 * Created by luc on 17/4/13.
 */
public enum ModelMsgStatus {

    DELAY(1),   //延迟出票消息
    TRANSATION(2);  //新油品交易出票消息

    private int index;

    ModelMsgStatus(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}
