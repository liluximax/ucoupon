## 微信公众平台 Java版

#### 该工程主要实现下列功能

* 微信公众平台 java后台工程 (网页OAuth2.0授权 jssdk开发 access_token jsapi_ticket缓存)

* 公众号内微信支付、使用优惠券和积分

* 公众号内发送模板消息

* 基于百度地图api，加油站地理信息标注

-------
#### 工程目录说明
1. /Weixin/src/com/spring包中主要描述公众号后台逻辑

1. /Weixin/src/com/weixin/pay包中主要描述微信支付及模板消息逻辑

2. /Weixin/src/com/weixin/qr包中主要描述生成场景二维码逻辑

3. /Weixin/WebContent中存放前端静态资源

4. /Weixin/resources中存放初始化加载的properties文件

5. documents中存放工程相关文档、接口规范

-------
#### 创新及改进
本工程在微信公众号开发文档和微信支付开发文档的基础上实现了两点改进和优化

> 使用双重通知策略确保支付成功之后业务逻辑的执行

支付成功之后，微信server会对notify_url进行回调，收到回调之后才可进行支付成功后的业务逻辑处理。官方给出的通知策略是15/30/180/1800/3600秒（重复通知需做缓存处理，避免业务逻辑重复执行）.

可是实际生产情况下，有千分之一的概率会出现半小时后微信才回调通知，从而产生了“漏单”。为了确保万无一失，增加前端页面通知策略：用户成功支付后，前端js会通知后台支付成功，此时controller10秒后检查该笔订单的回调是否到来，如果到了退出，如果没有到来，4分钟后再次检查，如果回调还是没有到来，则使用前端的支付成功通知（再调用微信查单API确保该笔订单的合法性）进行业务逻辑处理.

> 微信支付统一下单API的调用时间优化到300ms以内

在我使用的server上面，最初统一下单API可能偶尔出现长达15秒的调用时间，在server上执行curl命令，发现出现长时间等待的情况，是由于时间都花费在域名解析上面，强制使用ipv4进行域名解析可以解决这个问题。于是在tomcat启动参数中添加JVM设置，-Djava.net.preferIPv4Addresses，将调用时间优化到300ms以下.

-------
#### 页面展示
<div style="text-align:center">
	<div style="margin:10px 10px 10px 10px">
		<span><img src="http://oggznw7lj.bkt.clouddn.com/WechatIMG29.jpeg?imageView2/3/w/350/h/200/q/100"></span>
		<span><img src="http://oggznw7lj.bkt.clouddn.com/WechatIMG28.jpeg?imageView2/3/w/350/h/200/q/100"></span>
		<span><img src="http://oggznw7lj.bkt.clouddn.com/WechatIMG27.jpeg?imageView2/3/w/350/h/200/q/100"></span>
	</div>
	<hr>
	<div style="margin:10px 10px 10px 10px">
		<span><img src="http://oggznw7lj.bkt.clouddn.com/WechatIMG31.jpeg?imageView2/3/w/350/h/200/q/100"></span>
		<span><img src="http://oggznw7lj.bkt.clouddn.com/WechatIMG32.jpeg?imageView2/3/w/350/h/200/q/100"></span>
		<span><img src="http://oggznw7lj.bkt.clouddn.com/WechatIMG30.jpeg?imageView2/3/w/350/h/200/q/100"></span>
	</div>
</div>


